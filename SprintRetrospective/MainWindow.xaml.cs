﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SprintRetrospective
{
    public partial class MainWindow : Window
    {
        DBroutines dbr;
        static Data data = new Data();

        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = data;
            dbr = new DBroutines();

            //load data from database
            data.Stories = dbr.GetAllStoryPoints();
            data.Members = dbr.GetAllMembers();
        }

        void loadStories()
        {
            //delete all in data
            //foreach story for dbr, add to data
        }

        #region menucontrols
        private void HomeMenuButton_Click(object sender, RoutedEventArgs e)
        {
            data.SelectedPane.setActivePane(SelectedPane.Pane.Home);
        }

        private void StoryPointsMenuButton_Click(object sender, RoutedEventArgs e)
        {
            data.SelectedPane.setActivePane(SelectedPane.Pane.StoryPoints);
        }

        private void MembersMenuButton_Click(object sender, RoutedEventArgs e)
        {
            data.SelectedPane.setActivePane(SelectedPane.Pane.Members);
        }

        private void ProjectMenuButton_Click(object sender, RoutedEventArgs e)
        {
            data.SelectedPane.setActivePane(SelectedPane.Pane.Project);
        }

        private void SprintsMenuButton_Click(object sender, RoutedEventArgs e)
        {
            data.SelectedPane.setActivePane(SelectedPane.Pane.Sprints);
        }
        #endregion

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if (dbr != null)
                dbr.CloseConnection();
        }

        private void StoryPointsSaveButton_Click(object sender, RoutedEventArgs e)
        {
            dbr.SaveStories(data.Stories);
        }


    }

    
}
