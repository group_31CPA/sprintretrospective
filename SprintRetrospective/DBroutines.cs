﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IBM.Data.DB2;

namespace SprintRetrospective
{
    class DBroutines
    {
        Data data = new Data();

        //use this class to create queries, use DB2util class to run them
        public DBroutines()
        {
            DB2util.Connect();
        }

        public void CloseConnection()
        {
            DB2util.Close();
        }

        #region memberroutines
        public ObservableCollection<Member> GetAllMembers()
        {
            ObservableCollection<Member> members = new ObservableCollection<Member>();


            return members;
        }

        public bool AddMember()
        {


            return false;
        }

        public bool DeleteMember()
        {
            return false;
        }
        #endregion

        #region storypointroutines
        public ObservableCollection<Story> GetAllStoryPoints()
        {
            ObservableCollection<Story> points = new ObservableCollection<Story>();
            List<string> rows = new List<string>();

            if ((rows = DB2util.Select("SELECT * FROM STORIES")) == null)
                return points;

            string[] storyArr;
            foreach(string s in rows)
            {
                storyArr = s.Split(',');

                if(storyArr.Length < 10)
                {
                    Console.WriteLine("Not enough fields: SELECT * FROM STORIES;");
                    break;
                }

                points.Add(
                    new Story(
                        storyArr[1],
                        storyArr[2],
                        new Project(storyArr[3]),   //get from projects
                        new Sprint(storyArr[4]),
                        storyArr[5] != "null" ? int.Parse(storyArr[5]) : 0,
                        storyArr[6] != "null" ? int.Parse(storyArr[6]) : 0,
                        storyArr[7] != "null" ? int.Parse(storyArr[7]) : 0,
                        new Member(storyArr[8]),        //get from members
                        storyArr[9]
                        )
                    );
            }

            data.Stories = points;
            return points;
        }

        public ObservableCollection<Sprint> GetAllSprints()
        {
            ObservableCollection<Sprint> sprints = new ObservableCollection<Sprint>();
            List<string> rows = new List<string>();

            if ((rows = DB2util.Select("SELECT * FROM SPRINTS")) == null)
                return sprints;

            string[] sprintArr;
            foreach (string s in rows)
            {
                sprintArr = s.Split(',');

                if (sprintArr.Length < 10)
                {
                    Console.WriteLine("Not enough fields: SELECT * FROM SPRINTS;");
                    break;
                }

                sprints.Add(
                    new Sprint(
                        new Project(sprintArr[1]),//get from projects
                        sprintArr[2],
                        sprintArr[3],   
                        sprintArr[4]
                        )
                    );
            }

            return sprints;

        }

        internal void SaveStories(ObservableCollection<Story> stories)
        {
            List<Story> dbStories = new List<Story>();
            dbStories = this.GetAllStoryPoints().ToList();
            List<Story> storiesToDelete = new List<Story>();
            List<Story> storiesToAdd = new List<Story>();

            foreach(Story s in stories)
            {
                //find story in dbStories, if exists do nothing, if !exists add to add list
            }

            foreach(Story s in dbStories)
            {
                //find story in stories, if exists do nothing, if !exists add to delete list list

            }

            if(storiesToAdd.Count > 0)
            {
                foreach(Story s in storiesToAdd)
                {
                    //insert story
                }
            }

            if (storiesToDelete.Count > 0)
            {
                foreach (Story s in storiesToDelete)
                {
                    //delete story
                }
            }

        }

        #endregion

        #region projectroutines

        public ProjectDB2Model FindProjectByTeamNumber(string number)
        {
            string selectString = "SELECT * from Projects WHERE Projects.Team_Number = '" + number + "'";
            
            List<string> querry = DB2util.Select(selectString);
            ProjectDB2Model project = new ProjectDB2Model();
            foreach(string field in querry)
            {
                string[] fields = field.Split(',');
                project.id = long.Parse(fields[0]);
                project._teamName = fields[1];
                project._teamNumber = fields[2];
                project._productName = fields[3];
                project._accountLink = fields[4];
                project._startDate = fields[5];
                project._initialVelocity = long.Parse(fields[6]);
                project._hourStoryPoints = float.Parse(fields[7]);
            }            
            return project;
            
        }

        public void UpdateProject(ProjectDB2Model projectModel)
        {
            string updateString = "UPDATE Projects SET Team_Name = '" + projectModel._teamName +
                "' , Team_Number = '" + projectModel._teamNumber +
                "' , Product_Name = '" + projectModel._productName +
                "' , Account_Link = '" + projectModel._accountLink +
                "' , Start_Date = '" + projectModel._startDate +
                "' , Initial_Velocity = " + projectModel._initialVelocity +
                " , Hour_Per_Story_Point = " + projectModel._hourStoryPoints +
                " WHERE Project_ID = " + projectModel.id;
            DB2util.Update(updateString);
        }

        #endregion
    }
}
