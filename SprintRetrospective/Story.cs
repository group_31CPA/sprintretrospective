﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SprintRetrospective
{
    class Story : INotifyPropertyChanged
    {
        public Story()
        {

        }

        public Story(string name, string desc, Project project, Sprint sprint, int actual, int relative, int points, Member owner, string status)
        {
            Name = name;
            Description = desc;
            Project = project;
            Sprint = sprint;
            RelativeEstimate = relative;
            ActualTime = actual;
            Points = points;
            Owner = owner;
            Status = status;
        }

        private string name_;
        public string Name
        {
            get { return name_; }
            set
            {
                if (value != name_)
                {
                    name_ = value;
                    RaisePropertyChanged("Name");
                }
            }
        }

        private string desc_;
        public string Description
        {
            get { return desc_; }
            set
            {
                if (value != desc_)
                {
                    desc_ = value;
                    RaisePropertyChanged("Description");
                }
            }
        }

        private Member member_;
        public Member Member
        {
            get { return member_; }
            set
            {
                if (value != member_)
                {
                    member_ = value;
                    RaisePropertyChanged("Member");
                }
            }
        }

        private Project project_;
        public Project Project
        {
            get { return project_; }
            set
            {
                if (value != project_)
                {
                    project_ = value;
                    RaisePropertyChanged("Project");
                }
            }
        }

        private Sprint sprint_;
        public Sprint Sprint
        {
            get { return sprint_; }
            set
            {
                if (value != sprint_)
                {
                    sprint_ = value;
                    RaisePropertyChanged("Sprint");
                }
            }
        }

        private int relativeestimate_;
        public int RelativeEstimate
        {
            get { return relativeestimate_; }
            set
            {
                if(value != relativeestimate_)
                {
                    relativeestimate_ = value;
                    RaisePropertyChanged("RelativeEstimate");
                }
            }
        }

        private int actualtime_;
        public int ActualTime
        {
            get { return actualtime_; }
            set
            {
                if (value != actualtime_)
                {
                    actualtime_ = value;
                    RaisePropertyChanged("ActualTime");
                }
            }
        }

        private int points_;
        public int Points
        {
            get { return points_; }
            set
            {
                if (value != points_)
                {
                    points_ = value;
                    RaisePropertyChanged("points");
                }
            }
        }

        private Member owner_;
        public Member Owner
        {
            get { return owner_; }
            set
            {
                if (value != owner_)
                {
                    owner_ = value;
                    RaisePropertyChanged("Owner");
                }
            }
        }

        private string status_;
        public string Status
        {
            get { return status_; }
            set
            {
                if (value != status_)
                {
                    status_ = value;
                    RaisePropertyChanged("Status");
                }
            }
        }

        void RaisePropertyChanged(string prop)
        {
            if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs(prop)); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
