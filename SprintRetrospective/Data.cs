﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SprintRetrospective
{
    class Data
    { 
        public Data()
        {
            SelectedPane = new SelectedPane();
            project = new Project();
            routine = new DBroutines();
            SaveProjectInfoCommand = new RelayCommand(SaveProjectInfo);
            LoadProjectInfoCommand = new RelayCommand(LoadProjectInfo);
            project.TeamNumber = "0002";
            LoadProjectInfo(null);

        }

        public SelectedPane SelectedPane { get; set; }
        public ObservableCollection<Member> Members { get; set; }
        public ObservableCollection<Story> Stories{ get; set; }
        public ObservableCollection<Sprint> Sprints { get; set; }

        public Project project { get; set; }
        public DBroutines routine;

        public RelayCommand SaveProjectInfoCommand { get; set; }
        public void SaveProjectInfo(object parameter)
        {
            ProjectDB2Model projectModel = new ProjectDB2Model();
            projectModel.updateProjectFields(project);
            routine.UpdateProject(projectModel);            
        }

        public RelayCommand LoadProjectInfoCommand { get; set; }
        public void LoadProjectInfo(object parameter)
        {
            ProjectDB2Model projectModel = routine.FindProjectByTeamNumber(project.TeamNumber);
            project.updateProjectFields(projectModel);
        }

    }
}
