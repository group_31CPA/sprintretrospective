﻿using IBM.Data.DB2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SprintRetrospective
{
    static class DB2util
    {
        
        static string ConnectionString = "database=Group31-DEV;uid=sgh59682;pwd=078nxch62w16@b7k;";
        static DB2Connection Connection;
        

        public static void Connect()
        {
            Connection = new DB2Connection(ConnectionString);
            Connection.Open();            
        }

        //return rows as csv strings, call Split(',') to convert string to array
        public static List<string> Select(string query)
        {
            DB2Command MyDB2Command = null;

            MyDB2Command = Connection.CreateCommand();
            MyDB2Command.CommandText = query;
            Console.WriteLine(MyDB2Command.CommandText);
            DB2DataReader MyDb2DataReader = null;
            MyDb2DataReader = MyDB2Command.ExecuteReader();
            string resultString;
            List<string> rows = new List<string>();

            //read row
            while (MyDb2DataReader.Read())
            {
                resultString = "";
                //read columns
                for (int i = 0; i <= MyDb2DataReader.VisibleFieldCount; i++)
                {
                    //separate columns by commas
                    if (i != 0)
                        resultString += ",";

                    try
                    {
                        if (MyDb2DataReader.IsDBNull(i))
                        {
                            resultString += "null";
                            //return null;
                        }
                        else
                        {
                            resultString += MyDb2DataReader.GetString(i);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.Write(e.ToString());
                    }
                }

                rows.Add(resultString);

            }
            MyDb2DataReader.Close();
            MyDB2Command.Dispose();

            if (rows.Count != 0)
                return rows;
            else
                return null;
        }

        //return bool?
        public static void Insert()
        {
            if (Connection == null)
                Connect();


        }

        //return bool?
        public static void Update(string commandString)
        {
            if (Connection == null || !Connection.IsOpen)
                Connect();
            DB2Command Command = Connection.CreateCommand();
            Command.CommandText = commandString;
            try
            {
                Command.ExecuteReader();
            }catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        //return bool?
        public static void Delete()
        {
            if (Connection == null)
                Connect();

        }

        public static void Close()
        {
            if (Connection != null)
                Connection.Close();
        }

        private static void TestDatabase()
        {
            DB2Command MyDB2Command = null;

            MyDB2Command = Connection.CreateCommand();
            MyDB2Command.CommandText = "SELECT * FROM STORIES";
            Console.WriteLine(MyDB2Command.CommandText);

            DB2DataReader MyDb2DataReader = null;
            MyDb2DataReader = MyDB2Command.ExecuteReader();
            Console.WriteLine("============================");
            while (MyDb2DataReader.Read())
            {
                for (int i = 0; i <= 5; i++)
                {
                    try
                    {
                        if (MyDb2DataReader.IsDBNull(i))
                        {
                            Console.Write("NULL");
                        }
                        else
                        {
                            Console.Write(MyDb2DataReader.GetString(i));
                        }
                    }
                    catch (Exception e)
                    {
                        Console.Write(e.ToString());
                    }
                    Console.Write("\t");
                }
                //Console.Write(MyDb2DataReader.GetInt32(6).ToString());
                //Console.Write(" ");
                //Console.Write(MyDb2DataReader.GetDouble(7).ToString());

                Console.WriteLine("");
            }
            MyDb2DataReader.Close();
            MyDB2Command.Dispose();
        }
    }
}
