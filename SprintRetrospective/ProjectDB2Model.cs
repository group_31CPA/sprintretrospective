﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SprintRetrospective
{
    public class ProjectDB2Model
    {
        public long id;
        public string _teamName;
        public string _teamNumber;
        public string _productName;
        public string _accountLink;
        public long _initialVelocity;
        public string _startDate;
        public float _hourStoryPoints;

        public void updateProjectFields(Project project)
        {
            id = project._id;
            _teamName = project.TeamName;
            _teamNumber = project.TeamNumber;
            _productName = project.ProductName;
            _accountLink = project.AccountLink;
            _initialVelocity = long.Parse(project.InitialVelocity);
            _startDate = project.StartDate;
            _hourStoryPoints = float.Parse(project.HourStoryPoints);
 
        }
    }
}
