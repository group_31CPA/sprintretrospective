﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SprintRetrospective
{
    public class Project : INotifyPropertyChanged
    {
  
        public void updateProjectFields(ProjectDB2Model projectModel)
        {
            _id = projectModel.id;
            TeamName = projectModel._teamName;
            TeamNumber = projectModel._teamNumber;
            ProductName = projectModel._productName;
            AccountLink = projectModel._accountLink;
            InitialVelocity = projectModel._initialVelocity.ToString();
            StartDate = projectModel._startDate;
            HourStoryPoints = projectModel._hourStoryPoints.ToString();
            /*
            RaisePropertyChanged("TeamName");
            RaisePropertyChanged("TeamNumber");
            RaisePropertyChanged("ProductName");
            RaisePropertyChanged("AccountLink");
            RaisePropertyChanged("InitialVelocity");            
            RaisePropertyChanged("StartDate");
            RaisePropertyChanged("HourStoryPoints");
            */
        }

        public long _id;
        public Project()
        {

        }

        public Project(string name)
        {
            ProductName = name;
            TeamName = "";
        }

        string _teamName;
        public string TeamName
        {
            get
            {
                return _teamName;
            }
            set
            {
                if (_teamName != value)
                {
                    _teamName = value;
                    RaisePropertyChanged("TeamName");
                }
            }
        }

        string _teamNumber;
        public string TeamNumber
        {
            get
            {
                return _teamNumber;
            }
            set
            {
                if (_teamNumber != value)
                {
                    _teamNumber = value;
                    RaisePropertyChanged("TeamNumber");
                }
            }
        }

        string _productName;
        public string ProductName
        {
            get
            {
                return _productName;
            }
            set
            {
                if (_productName != value)
                {
                    _productName = value;
                    RaisePropertyChanged("ProductName");
                }
            }
        }

        string _accountLink;
        public string AccountLink
        {
            get
            {
                return _accountLink;
            }
            set
            {
                if (_accountLink != value)
                {
                    _accountLink = value;
                    RaisePropertyChanged("AccountLink");
                }
            }
        }

        string _initialVelocity;
        public string InitialVelocity
        {
            get
            {
                return _initialVelocity;
            }
            set
            {
                if (_initialVelocity != value)
                {
                    _initialVelocity = value;
                    RaisePropertyChanged("InitialVelocity");
                }
            }
        }

        string _startDate;
        public string StartDate
        {
            get
            {
                return _startDate;
            }
            set
            {
                if (_startDate != value)
                {
                    _startDate = value;
                    RaisePropertyChanged("StartDate");
                }
            }
        }

        string _hourStoryPoints;
        private string v;



        public string HourStoryPoints
        {
            get
            {
                return _hourStoryPoints;
            }
            set
            {
                if (_hourStoryPoints != value)
                {
                    _hourStoryPoints = value;
                    RaisePropertyChanged("HourStoryPoints");
                }
            }
        }

        public void RaisePropertyChanged(string prop)
        {
            if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs(prop)); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public override string ToString()
        {
            return this.TeamName + ": " + this.ProductName;
        }
    }
}
