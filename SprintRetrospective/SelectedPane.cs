﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SprintRetrospective
{
    class SelectedPane : INotifyPropertyChanged
    {
        public enum Pane { Home, StoryPoints, Members, Project, Sprints };

        Visibility homePane;
        public Visibility HomePane
        {
            get { return homePane; }
            set
            {
                if (homePane != value)
                {
                    homePane = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("HomePane"));
                }
            }
        }

        Visibility projectPane;
        public Visibility ProjectPane
        {
            get { return projectPane; }
            set
            {
                if (projectPane != value)
                {
                    projectPane = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ProjectPane"));
                }
            }
        }

        Visibility storyPointsPane;
        public Visibility StoryPointsPane
        {
            get { return storyPointsPane; }
            set
            {
                if (storyPointsPane != value)
                {
                    storyPointsPane = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("StoryPointsPane"));
                }
            }
        }

        Visibility membersPane;
        public Visibility MembersPane
        {
            get { return membersPane; }
            set
            {
                if (membersPane != value)
                {
                    membersPane = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("MembersPane"));
                }
            }
        }

        Visibility sprintPane;
        public Visibility SprintPane
        {
            get { return sprintPane; }
            set
            {
                if (sprintPane != value)
                {
                    sprintPane = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("SprintPane"));
                }
            }
        }



        public event PropertyChangedEventHandler PropertyChanged;

        public SelectedPane()
        {
            setAllPanesFalse();
            HomePane = Visibility.Visible;
        }

        public void setActivePane(Pane pane)
        {
            if (pane == Pane.Home)
            {
                setAllPanesFalse();
                HomePane = Visibility.Visible;
            }
            else if (pane == Pane.StoryPoints)
            {
                setAllPanesFalse();
                StoryPointsPane = Visibility.Visible;
            }
            else if (pane == Pane.Members)
            {
                setAllPanesFalse();
                MembersPane = Visibility.Visible;
            }
            else if (pane == Pane.Project)
            {
                setAllPanesFalse();
                ProjectPane = Visibility.Visible;
            }
            else if (pane == Pane.Sprints)
            {
                setAllPanesFalse();
                SprintPane = Visibility.Visible;
            }

        }

        private void setAllPanesFalse()
        {
            HomePane = Visibility.Hidden;
            StoryPointsPane = Visibility.Hidden;
            MembersPane = Visibility.Hidden;
            ProjectPane = Visibility.Hidden;
            SprintPane = Visibility.Hidden;
        }
    }
}
