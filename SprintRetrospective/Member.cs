﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SprintRetrospective
{
    class Member : INotifyPropertyChanged
    {
        public Member()
        {

        }

        public Member(string firstname)
        {
            FirstName = firstname;
        }

        private string firstname_;
        public string FirstName
        {
            get { return firstname_; }
            set
            {
                if(firstname_ != value)
                {
                    firstname_ = value;
                    RaisePropertyChanged("FirstName");
                }
            }
        }

        private string lastname_;
        public string LastName
        {
            get { return lastname_; }
            set
            {
                if (lastname_ != value)
                {
                    lastname_ = value;
                    RaisePropertyChanged("LastName");
                }
            }
        }

        //add other fields here


        //tostring method
        public override string ToString()
        {
            return FirstName + " " + LastName;
        }

        void RaisePropertyChanged(string prop)
        {
            if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs(prop)); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
