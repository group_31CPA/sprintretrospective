﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SprintRetrospective
{
    class Sprint : INotifyPropertyChanged
    {
        public Sprint()
        {

        }
        public Sprint(string name)
        {
            Name = name;
        }

        public Sprint(Project project, string name, string start, string end)
        {
            Project = project;
            Name = name;
            StartDate = start;
            EndDate = end;
        }

        public int id { get; set; }

        private Project project_;
        public Project Project
        {
            get { return project_; }
            set
            {
                if (project_ != value)
                {
                    project_ = value;
                    RaisePropertyChanged("Project");
                }
            }
        }

        private string name_;
        public string Name
        {
            get { return name_; }
            set
            {
                if (name_ != value)
                {
                    name_ = value;
                    RaisePropertyChanged("Name");
                }
            }
        }

        private string startdate_;
        public string StartDate
        {
            get { return startdate_; }
            set
            {
                if (startdate_ != value)
                {
                    startdate_ = value;
                    RaisePropertyChanged("StartDate");
                }
            }
        }

        private string enddate_;
        public string EndDate
        {
            get { return enddate_; }
            set
            {
                if (enddate_ != value)
                {
                    enddate_ = value;
                    RaisePropertyChanged("EndDate");
                }
            }
        }

        void RaisePropertyChanged(string prop)
        {
            if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs(prop)); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
